# Puppeteer

Puppeteer on docker from [Official Github](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#running-puppeteer-in-docker)

Image repositories:

* [registry.gitlab.com/savadenn-public/puppeteer:latest](https://gitlab.com/savadenn-public/puppeteer/container_registry)
* [savadenn/puppeteer:latest](https://hub.docker.com/r/savadenn/puppeteer)